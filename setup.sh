#!/bin/bash

#Check AWS conf
echo -n "AWS credentials... "
if [[ ! -s ~/.aws/credentials ]]; then
    echo "Error: AWS credentials were not found !"
    exit
else
    echo "OK"
fi

#Install python dependencies
echo -n "Install system-wide python3 dependencies... "
sudo apt install -y python3-pip python3-venv &>/dev/null
if [[ $? > 0 ]]; then
    echo "Error : Could not install dependencies (make sure you have privileges) !"
    exit
else
    echo "OK"
fi

#Create virtual environment
echo -n "Create python virtual environment... "
err=0
python3 -m venv ~/.cbdproject &>/dev/null;    [[ $? > 0 ]] && err=$?
source ~/.cbdproject/bin/activate;                [[ $? > 0 ]] && err=$?
if [[ $err > 0 ]]; then
    echo "Error : Could not use virtual environment !"
    exit
else
    echo "OK"
fi

#Upgrade pip and install dependencies
echo -n "Install venv-wide dependencies... "
err=0
python -m pip install --upgrade pip &>/dev/null;         [[ $? > 0 ]] && err=$?
python -m pip install -r requirements.txt &>/dev/null;   [[ $? > 0 ]] && err=$?
if [[ $err > 0 ]]; then
    echo "Error : Could not install requirements !"
    exit
else
    echo "OK"
fi

#Create AWS cluster
python ec2_setup.py

#Deploy Kubernetes
python deploy-kubernetes.py

#Deploy kube-opex-analytics tool
python deploy-koa.py

#Deploy Hadoop and Spark
python deploy-spark.py

#Execute an application on spark
python exec-spark.py