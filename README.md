# CBD Project
Welcome to the CBD Project (Cloud x Big Data) !

## Purpose
The purpose of this project is to automate the deployment of Kubernetes on a AWS EC2 cluster and to monitor the resources usage of a very simple Apache Spark application (WordCount).

## Requirements
In order to execute it, you must:
- **Have an AWS account**
- **Configure a proper programmatic access** through its API
  - To do so, please refer to the official documentation: https://docs.aws.amazon.com/general/latest/gr/aws-general.pdf.
- **Configure your AWS CLI** (the file `~/.aws/credentials` must be filled with the previously generated programmatic access keys).
  - To do so, please refer to the official documentation: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html.
- **Have Docker Engine and Docker Compose installed**
  - To do so, please refer to the official documentation depending on your GNU Linux distribution. 

## Execution
In order to get the CBD-Project, execute the following commands:
```bash
git clone https://gitlab.com/raph.berteaud/cbd-project.git
cd cbd-project
```
There are two ways to start the project:
- **Via a docker container:**
>NB: In this way, the project will automatically teardown after 20 minutes.
```bash
docker-compose up
```

If you have any trouble, please consider adding user to the Docker group:
```bash
sudo usermod -aG docker $USER
sudo reboot
```

To remove both container and image resulting from previous commands:
```bash
docker-compose down --rmi all
```

- **Via shell scripts:**

Start project:
```bash
source setup.sh
```

teardown project:
```bash
source teardown.sh
```

## Note
By default, this project is using t2.medium instance type from EC2, thereby, it could implies additional costs.

To change parameters of the project such as the type of instance, you can modify variables in the begining of script `ec2_setup.py`.

To change parameters of the Spark execution, you can modify variables in the begining of script `exec-spark.py`.