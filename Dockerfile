FROM ubuntu
WORKDIR /cbd
COPY . .
RUN apt-get update && \
    apt-get upgrade -y && \
    apt install sudo -y && \
    chmod -R 777 ./
ENTRYPOINT ./setup.sh && \
    sleep 1200 && \
    ./teardown.sh &&\
    exit