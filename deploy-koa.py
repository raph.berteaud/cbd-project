"""
Purpose

Deploy kube-opex-analytics on the existing Kubernetes cluster master node (on top of AWS EC2 cluster).
"""

import utils
from fabric import Connection
import os

def install_kube_metrics_server(kube_master):
    """
    Install Kube Metrics Server (requirement for kube-opex-analytics)
    """
    assert kube_master.is_connected

    kube_master.run("kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml", hide="both")

def expose_kube_api(kube_master, port=8080):
    """
    Publish kubectl REST API on localhost:[port=8080]
    """
    assert isinstance(port, int)
    assert port > 1024 and port < 65535
    assert kube_master.is_connected

    #Trick to run command in background
    kube_master.run(f"nohup kubectl proxy --port={port} &>/dev/null &", hide="both", pty=False)

def patch_kube_metrics_server(kube_master):
    """
    Patch Kube Metrics Server to overcome "Service Unavailable" issue...
    (bigoof black magic voodoo stuff)
    https://github.com/kubernetes-sigs/metrics-server/issues/188
    """
    assert kube_master.is_connected

    # For debug purposes, the complete command (to run through ssh) is :
    # kubectl patch deployment -n kube-system metrics-server --type=json -p="[{\"op\": \"replace\", \"path\": \"/spec/template/spec/containers/0/args\", \"value\": [\"--cert-dir=/tmp\", \"--secure-port=4443\", \"--kubelet-preferred-address-types=InternalIP,Hostname,InternalDNS\", \"--kubelet-use-node-status-port\", \"--kubelet-insecure-tls=true\"]}]"
    kube_master.run("kubectl patch deployment " +
                        "-n kube-system metrics-server --type=json " +
                        "-p='[{\"op\": \"replace\", \"path\": \"/spec/template/spec/containers/0/args\", " +
                        "\"value\": [" +
                        "\"--cert-dir=/tmp\", " +
                        "\"--secure-port=4443\", "+
                        "\"--kubelet-preferred-address-types=InternalIP,Hostname,InternalDNS\", " +
                        "\"--kubelet-use-node-status-port\", " +
                        "\"--kubelet-insecure-tls=true\"]}]'",
                    hide="both")

    # For debug purposes, the complete command (to run through ssh) is :
    # kubectl patch deployment -n kube-system metrics-server -p="{\"spec\": {\"template\": {\"spec\": {\"hostNetwork\":true}}}}"
    kube_master.run("kubectl --request-timeout '0' patch deployment " +
                        "-n kube-system metrics-server " +
                        "-p '{\"spec\": {\"template\": {\"spec\": {\"hostNetwork\":true}}}}'",
                    hide="both")
    
    #TODO : Maybe find a way to wait for `kubectl top node` to respond

def run_koa(kube_master):
    """
    Run rchakode/kube-opex-analytics container
    (web interface accessible via localhost:5483)
    """
    assert kube_master.is_connected

    kube_master.run("docker run -d " + 
        "--net='host' " +
        "--name 'kube-opex-analytics' " +
        "-v /var/lib/kube-opex-analytics:/data " +
        "-e KOA_DB_LOCATION=/data/db " +
        "-e KOA_K8S_API_ENDPOINT=http://127.0.0.1:8080 " +
        "rchakode/kube-opex-analytics",
        hide="both"
    )

def redirect_80_to_5483(koa_host):
    """
    Redirect KOA Host port 80 to port 5483
    to make KOA web interface accessible via http://[koa_host] directly
    """
    assert koa_host.is_connected

    koa_host.sudo("iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5483")

def deploy_koa(master):
    """
    Install kube-opex-analytics on k8s master node.
    :param master: The master instance of the cluster.
    :param subnet: The private subnet of the cluster.
    """

    masterClient = utils.connect_ssh(master.public_ip_address)

    print("Installing Kube Metrics Server... ", end='', flush=True)
    install_kube_metrics_server(masterClient)
    print("Done")

    kube_api_port = 8080
    print(f"Exposing Kubernetes REST API on localhost:{kube_api_port}... ", end='', flush=True)
    expose_kube_api(masterClient, kube_api_port)
    print("Done")

    print("Patching Kube Metrics Server deployment... ", end='', flush=True)
    patch_kube_metrics_server(masterClient)
    print("Done")

    print("Starting rchakode/kube-opex-analytics container... ", end='', flush=True)
    run_koa(masterClient)
    print("Done")

    print("Redirecting ports... ", end='', flush=True)
    redirect_80_to_5483(masterClient)
    print("Done")

    masterClient.close()
    print(f"SSH session to host unbuntu@{ master.public_ip_address } closed") 


if __name__ == '__main__':
    print("="*88)
    print("Starting deployment of kube-opex-analytics.")
    print("="*88)

    vm_instances = utils.get_running_instances()
    master = utils.get_master(vm_instances)

    deploy_koa(master)

    print("kube-opex-analytics deployed successfully !")
    print(f"\tkube-opex-analytics web interface available at http://{ master.public_dns_name }")
    print("\tYou might wanna grab a coffee and pray for k8s to cooperate...")